# ultrasoccer

Diese App ist "nur" eine Weiterleitung zum Browserspiel ultrasoccer.de, beinhaltet das Spiel selbst aber nicht.

Als Mitspieler und Teil der Ultrasoccer-Community habe ich unter Rücksprache mit dem Eigentümer und Entwickler von ultrasoccer.de diese App erstellt, um das kostenlose und werbefreie Fußballmanager-Spiel auch im Open Store verfügbar zu machen.

## License

Copyright (C) 2022  Tony Ford

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
